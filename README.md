# Initial Install
```bash
curl https://sh.rustup.rs -sSf sh && source $HOME/.cargo/env && cargo install mdbook && mdbook build --open
```
# Build and Open
```bash
mdbook build --open
```