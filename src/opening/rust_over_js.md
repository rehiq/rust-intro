## WHY "LOW-LEVEL, BUT WITH 0 COST ABSTRACTION"

_Predictable performance_ for **control** over e.g. memory layout, collection. 

If you have any type of code that has to have a predictable performance, this is where you go at. 

## WHY "GUARANTEED MEMORY SAFETY"

Manually ensuring data integrity is **tough**. 

## WHY "THREADS WITHOUT SAFETY"

Manually ensuring data integrity with concurrent access is __inhuman__.

## WHY "TYPE INFERENCE"

Types are there for performance (generally) or correctness(TS), but with RUST it is **both**.

```rust
fn main() {
	// because of this annotation the compiler will know that `elem` has type i8;
	let elem = 5i8;
	// Create a empty vector - array that can change
	let mut vec = Vec::new();
	// infer from what we insert
	vec.push(elem);
	println!("{:?}", vec);
}
```

Minimal type annotations of variables are needed.
Try to make the computer work for you.

## WHY "MINIMAL RUNTIME"

>At present, Go generates large Wasm files, with the smallest possible size being around ~2MB. If your Go code imports libraries, this file size can increase dramatically. 10MB+ is common.

## WHY "PATTERN PATCHING"

```rust
fn main() {
	let greeting = ["Здрасти", "猪先生", "السيد خنزير"];
	
	for (num, greeting) in greeting.iter().enumerate() {
		println!("{}", greeting);
		match num {
			0 => println!("Този код може да се променя и пуска. Енджой."),
			1 => println!("可以修改和播放此代码。好好享受"),
			2 => println!("يمكن تعديل هذا الرمز وتشغيله. التمتع بها."),
			// _ => println!("Any other way")
		}
	}
}
```
