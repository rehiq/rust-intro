## Resources

### Read the books

* [Rust-Language](https://doc.rust-lang.org/book/)
* [Do the rustling course!](https://github.com/rust-lang/rustlings/)
* [Or Rust by Example](https://doc.rust-lang.org/stable/rust-by-example/)
* [Standard Library](https://doc.rust-lang.org/std/index.html)
* [Cargo book](https://doc.rust-lang.org/cargo/index.html)
* [RUST WASM Book](https://rustwasm.github.io/docs/book/)
* [Embedded Book](https://rust-embedded.github.io/book/)
* [Cheat sheet](https://cheats.rs/#data-structures)
* [Even more](https://github.com/ctjhoa/rust-learning)
* [Are we ... yet?](https://github.com/ctjhoa/rust-learning#are-we--yet)


Rust WASM frameworks out there:
 * [Yew](https://github.com/yewstack/yew)

Server frameworks:
* [Actix-Web](https://actix.rs/) - [massive](https://www.techempower.com/benchmarks/#section=data-r18)
* [Rocket](https://rocket.rs/)

Db DSL:
* [Diesel](http://diesel.rs/)
