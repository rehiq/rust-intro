# The Opening Pitch

Working with Rust (in the beginning) is like banging your head against the wall. 

## Expectations

Rust will **_always_** be longer, more verbose  and more complex than JS.
It is better to know this in advance rather than being disappointed.

## The Rust-lang and Why Rust?

> A language empowering everyone to build reliable and efficient software.

Performance | Reliability | Productivity
------------ | ------------- | ------------ 
Rust is blazingly fast and memory-efficient: with no runtime or garbage collector, it can power performance-critical services, run on embedded devices, and easily integrate with other languages. | Rust’s rich type system and ownership model guarantee memory-safety and thread-safety — and enable you to eliminate many classes of bugs at compile-time. | Rust has great documentation, a friendly compiler with useful error messages, and top-notch tooling — an integrated package manager and build tool, smart multi-editor support with auto-completion and type inspections, an auto-formatter, and more.

## Rust over JS
* low-level, but with 0 cost abstraction
* guaranteed memory safety
* threads without races
* type inference
* pattern matching (exhaustive)
* minimal runtime

