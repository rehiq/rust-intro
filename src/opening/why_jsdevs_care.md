# Why would JS devs care? -WASM

__Warning__ Using wasm is uncomfortable and unidiomatic by default.

We should combine JavaScript and Rust for better performance and enable reusing of already written code.

High level approaches to using WASM in the Browser:
1) Naive
2) NON-Naive

WASM on the server:
1) WASI ([article](https://hacks.mozilla.org/2019/03/standardizing-wasi-a-webassembly-system-interface/),[official](https://wasi.dev/))
## Naive Approach
* Javascript initializes
* Fetch and load Rust Wasm
* wasm exposes a bridge of functions for JS to call
* JS calls wasm functions which return e.g. string

### Problem?!

Sending data JS -> WASM by default involves allocations - something we initially wanted to avoid.

### Goals

* We would try to expose an interface from the rust side that will minimize the copying, serialization and de-serialization.

## NON-Naive Approach

This is normally done with _pointers_.
* Pass opaque handlers to data a structure - Instead of serializing it on one side, copy it into some known location in the WASM linear mem.
* As a general rule of thumb, large, long-lived data structures are implemented as Rust types
	* They live in the Rust linear mem, and are exposed to js as opaque handlers.
	* JS calls exposed Rust functions with the opaque handlers.
	* Rust transforms the data, performs heavy computations, queries the data.
	* Rust returns a small, copy-able result.

## Goals

Try to use shared array buffers (shared mem)

![JS + RUST = "ВНЛ"](https://2r4s9p1yi1fa2jd7j43zph8r-wpengine.netdna-ssl.com/files/2018/03/05_wasm_bindgen_2-500x261.png)

```rust
mod utils;
mod timer;

use std::fmt;
use wasm_bindgen::prelude::*;
use timer::Timer;

#[wasm_bindgen]
#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Cell {
	Dead = 0,
	Alive = 1
}

impl Cell {
	fn toggle(&mut self) {
		*self = match *self {
			Cell::Dead => Cell::Alive,
			Cell::Alive => Cell::Dead,
		}
	}
}

#[wasm_bindgen]
pub struct Universe {
	width: u32,
	height: u32,
	cells: Vec<Cell>,
}

impl Universe {
	fn get_index(&self, row: u32, column: u32) -> usize {
		(row * self.width + column) as usize
	}

	fn live_neighbor_count(&self, row: u32, column: u32) -> u8 {
		let mut count = 0;
		for delta_row in [self.height - 1, 0 , 1].iter().cloned() {
			for delta_col in [self.width - 1, 0, 1].iter().cloned() {
				if delta_row == 0 && delta_col == 0 {
					continue;
				}

				let neightbor_row = (row + delta_row) % self.height;
				let neightbor_collumn = (column + delta_col) % self.width;
				let idx = self.get_index(neightbor_row, neightbor_collumn);
				count += self.cells[idx] as u8;
			}
		}
		count
	}

	/// Get the dead and alive values of the entire universe.
	pub fn get_cells(&self) -> &[Cell] {
		&self.cells
	}

	/// Set cells to be alive in the universe by passing the row and collumn
	/// of each cell as an array
	pub fn set_cells(&mut self, cells: &[(u32, u32)]) {
		for (row, col) in cells.iter().cloned()  {
			let idx = self.get_index(row, col);
			self.cells[idx] = Cell::Alive;
		}
	}
}

/// Public methods, exported to JavaScript.
#[wasm_bindgen]
impl Universe {

	pub fn width(&self) -> u32 {
		self.width
	}

    /// Set the width of the universe.
    ///
    /// Resets all cells to the dead state.
	pub fn set_width(&mut self, width: u32) {
		self.width = width;
		self.cells = (0..width * self.height).map(|_i| Cell::Dead).collect();
	}

	///Set the height of the universe.
	///
	/// Reset all cells to the dead state. 
	pub fn set_height(&mut self, height: u32) {
		self.height = height;
		self.cells = (0..self.width * height).map(|_i| Cell::Dead).collect();
	}

	pub fn height(&self) -> u32 {
		self.height
	}

	pub fn cells(&self) -> *const Cell {
		self.cells.as_ptr()
	}

	pub fn tick(&mut self) {
		let _timer = Timer::new("Universe::tick");
		let mut next = {
			let _timer = Timer::new("allocate next cells");
			self.cells.clone()
		};

		{
			let _timer = Timer::new("new generation");
			for row in 0..self.height {
				for col in 0..self.width {
					let idx = self.get_index(row, col);
					let cell = self.cells[idx];
					let live_neighbours = self.live_neighbor_count(row, col);

					// log!(
					// 	"Cell[{}, {}] is initially {:?} adn has {} live neighbours",
					// 	row,
					// 	col,
					// 	cell,
					// 	live_neighbours
					// );

					let next_cell = match (cell, live_neighbours) {
						(Cell::Alive, x) if x < 2 => Cell::Dead,
						(Cell::Alive, 2) | (Cell::Alive, 3) => Cell::Alive,
						(Cell::Alive, x) if x > 3 => Cell::Dead,
						(Cell::Dead, 3) => Cell::Alive,
						(otherwise, _) => otherwise,
					};

					// log!("    it becomes {:?}", next_cell);
					next[idx] = next_cell;
				}
			}
		}
		let _timer = Timer::new("free old cells");
		self.cells = next;
	}

	pub fn new() -> Universe {
		utils::set_panic_hook();
		let width = 128;
		let height = 128;

		let cells = (0..width * height)
			.map(|i| {
				if i % 2 == 0 || i % 7 == 0 {
					Cell::Alive
				} else {
					Cell::Dead
				}
			})
			.collect();

		Universe {
			width,
			height,
			cells,
		}
	}

	pub fn render(&self) -> String {
		self.to_string()
	}

	pub fn toggle_cell(&mut self, row: u32, column: u32) {
		let idx = self.get_index(row, column);
		self.cells[idx].toggle();
	}
}

impl fmt::Display for Universe {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		for line in self.cells.as_slice().chunks(self.width as usize) {
			for &cell in line {
				let symbol = if cell == Cell::Dead { '◻' } else { '◼' };
				write!(f, "{}", symbol)?;
			}
			write!(f, "\n")?;
		}

		Ok(())
	}
} 
```

The generated package underneath is an NPM package

```javascript
/* tslint:disable */
export enum Cell {
  Dead,
  Alive,
}
/**
*/
/**
*/
export class Universe {
  free(): void;
/**
* @returns {number} 
*/
  width(): number;
/**
* Set the width of the universe.
*
* Resets all cells to the dead state.
* @param {number} width 
*/
  set_width(width: number): void;
/**
*Set the height of the universe.
*
* Reset all cells to the dead state. 
* @param {number} height 
*/
  set_height(height: number): void;
/**
* @returns {number} 
*/
  height(): number;
/**
* @returns {number} 
*/
  cells(): number;
/**
*/
  tick(): void;
/**
* @returns {Universe} 
*/
  static new(): Universe;
/**
* @returns {string} 
*/
  render(): string;
/**
* @param {number} row 
* @param {number} column 
*/
  toggle_cell(row: number, column: number): void;
}

```

Using the memory from JS:
```javascript
import { memory } from "wasm-game-of-life/wasm_game_of_life_bg"

....

const drawCells = () => {
	const cellsPtr = universe.cells();
	const cells = new Uint8Array(memory.buffer, cellsPtr, width * height);

	ctx.beginPath();

	ctx.fillStyle = ALIVE_COLOR;
	for (let row = 0; row < height; row++) {
		for (let col = 0; col < width; col++) {
			const idx = getIndex(row, col);
			if (cells[idx] !== Cell.Alive) {
				continue;
			}

			ctx.fillRect(
				col * (CELL_SIZE + 1) + 1,
				row * (CELL_SIZE + 1) + 1,
				CELL_SIZE,
				CELL_SIZE
			);
		}
	}

	// Dead cells.
	ctx.fillStyle = DEAD_COLOR;
	for (let row = 0; row < height; row++) {
		for (let col = 0; col < width; col++) {
			const idx = getIndex(row, col);
			if (cells[idx] !== Cell.Dead) {
				continue;
			}

			ctx.fillRect(
				col * (CELL_SIZE + 1) + 1,
				row * (CELL_SIZE + 1) + 1,
				CELL_SIZE,
				CELL_SIZE
			);
		}
	}
	ctx.stroke();
}
```
