# Debugging

## Who, When and Where

The Language > The Compiler > The Linter > __The Tests__ > __Ze Dev__ > the user

If we could eliminate a whole variety of errors just in the language then this is worth it.
The compiler can _read_ out intentions and tell (pinpoint) to places that we could have potential bugs - forget a case, miss supplied a type in a function etc.

Rust pushes the debugging experience _up front_. (Very painful for me)

Very common (not universal) is that a compiling program "just works".

With JS you get something working very quickly. It will be in the browser, you can see it and you can interact with it, but when the bugs happen, then do _further away_.