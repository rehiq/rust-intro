# Summary Rust General Introduction

[Who am I](./whoami.md)
- [Opening Pitch](./opening/opening_pitch.md)
- [Rust Over JS](./opening/rust_over_js.md)
- [Debugging](./opening/debugging.md)
- [Why Care?](./opening/why_jsdevs_care.md)
- [Resources](./opening/resources.md)
